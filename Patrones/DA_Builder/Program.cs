﻿using System;
using DA_Builder.Clases;

namespace DA_Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaBuilder Pizzeria = new PizzaItalianaBuilder();
            Pizza p = Pizzeria.BuildPizza();
            Console.WriteLine(p.ToString());
        }
    }
}
