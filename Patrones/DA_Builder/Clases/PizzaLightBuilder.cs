﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_Builder.Clases
{
    class PizzaLightBuilder : PizzaBuilder
    {
        public PizzaLightBuilder()
        {
            _descripcion = "Pizza Light";
        }
        public override Agregado BuildAgregado()
        {
            return new Berengenas();
        }

        public override Masa BuildMasa()
        {
            return new Integral();
        }

        public override Salsa BuildSalsa()
        {
            return new Light();
        }
    }
}
