﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_Builder.Clases
{
    public abstract class PizzaBuilder
    {
        protected string _descripcion;
        public abstract Masa BuildMasa();
        public abstract Salsa BuildSalsa();
        public abstract Agregado BuildAgregado();
        public override string ToString()
        {
            return _descripcion;
        }
        public Pizza BuildPizza()
        {
            Masa masa = BuildMasa();
            Salsa salsa = BuildSalsa();
            Agregado agregado = BuildAgregado();

            return new Pizza(masa: masa,
                                salsa: salsa,
                                agregado: agregado,
                                tipo: _descripcion);
        }
    }
}
