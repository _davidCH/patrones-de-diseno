﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_Builder.Clases
{
    public abstract class Masa
    {
        protected string _descripcion;
        public string Descripcion
        {
            get
            {
                return _descripcion;
            }
        }
    }
    public class AlMolde : Masa
    {
        public AlMolde()
        {
            _descripcion = "Masa al molde";
        }
    }
    public class ALaPiedra : Masa
    {
        public ALaPiedra()
        {
            _descripcion = "Masa a la piedra del horno a la leña";
        }
    }
    public class Integral : Masa
    {
        public Integral()
        {
            _descripcion = "Masa de harina integral";
        }
    }

}
