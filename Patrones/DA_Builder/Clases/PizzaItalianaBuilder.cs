﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_Builder.Clases
{
    public class PizzaItalianaBuilder : PizzaBuilder 
    {
        public PizzaItalianaBuilder()
        {
            _descripcion = "Pizza Italiana";
        }
        public override Agregado BuildAgregado()
        {
            return new Anchoas();
        }

        public override Masa BuildMasa()
        {
            return new ALaPiedra();
        }

        public override Salsa BuildSalsa()
        {
            return new Oliva();
        }
    }
}
