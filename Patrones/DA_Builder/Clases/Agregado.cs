﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_Builder.Clases
{
    public abstract class Agregado
    {
        protected string _descripcion;
        public string Descripcion
        {
            get
            {
                return _descripcion;
            }
        }
    }
    public class Oregano : Agregado
    {
        public Oregano()
        {
            _descripcion = "Oregano fresco";
        }
    }
    public class Anchoas : Agregado
    {
        public Anchoas()
        {
            _descripcion = "Anchoa en aceite";
        }
    }
    public class Berengenas : Agregado
    {
        public Berengenas()
        {
            _descripcion = "Berengenas sin calorias";
        }
    }
}
