﻿using System;
using BA_FactoryMethod.Clases;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            BebidaEmbriagante oBebida = Creador.CreadorBebida(Creador.CERVEZA);
            Console.WriteLine(oBebida.CuantoEmbriagaPoHora()) ;
        }
    }
}
