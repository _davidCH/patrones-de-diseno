﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BA_FactoryMethod.Clases
{
    class VinoTinto : BebidaEmbriagante
    {
        public override int CuantoEmbriagaPoHora()
        {
            return 20;
        }
    }
}
