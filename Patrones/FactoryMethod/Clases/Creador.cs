﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BA_FactoryMethod.Clases
{
    public class Creador
    {
        public const int VINO_TINTO = 1;
        public const int CERVEZA = 2;
        public static BebidaEmbriagante CreadorBebida(int Tipo) //la creación de la instancia se delega a esta clase, el desarrollados no sabe que objeto se ha creado , solo le interesa saber el nombre del método 
        {
            switch (Tipo)
            {
                case VINO_TINTO:
                    return new VinoTinto();
                case CERVEZA:
                    return new Cerveza();
                default: return null;
            }
        }
    }
}
