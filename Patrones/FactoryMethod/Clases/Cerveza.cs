﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BA_FactoryMethod.Clases
{
    class Cerveza : BebidaEmbriagante
    {
        public override int CuantoEmbriagaPoHora()
        {
            return 5;
        }
    }
}
