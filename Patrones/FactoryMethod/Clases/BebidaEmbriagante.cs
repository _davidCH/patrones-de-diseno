﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BA_FactoryMethod.Clases
{
    public abstract class BebidaEmbriagante
    {
        public abstract int CuantoEmbriagaPoHora();
    }
}
