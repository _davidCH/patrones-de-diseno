﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AA_Singleton.Clase
{
    public class Singleton
    {
        private static Singleton _instance = null;
        protected Singleton()
        {
            Mensaje = "Singleton";
        }
        public static Singleton Instance
        {
            get
            {
                if(_instance == null)
                    _instance = new Singleton();
                return _instance;
            }
        }
        public string Mensaje;
    }
}
