﻿using System;
using AA_Singleton.Clase;
namespace AA_Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //Singleton data = new Singleton(); //No se puede crear una instancia ya que el constructor es protected
            Console.WriteLine(Singleton.Instance.Mensaje);
            Singleton.Instance.Mensaje = "Uso segunda vez";
            Console.WriteLine(Singleton.Instance.Mensaje);
        }
    }
}
