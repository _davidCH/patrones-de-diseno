﻿using System;
using EA_Prototype.Clase;

namespace EA_Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            AutoPrototype P_fiat = new FiatPrototype();
            AutoPrototype P_DS = new DSPrototype();
            AutoPrototype P_alfa = new AlfaRomeoPrototype();

            AutoPrototype fiatPalio = P_fiat.Clonar();
            fiatPalio.Modelo = "Palio Fire";
            fiatPalio.Color = "negro";
            

            AutoPrototype fiatUno = P_fiat.Clonar();
            fiatUno.Modelo= "Uno SRC";
            fiatUno.Color = "Blanco";

            Console.WriteLine(fiatPalio.VerAuto());
            Console.WriteLine(fiatUno.VerAuto());
        }
    }
}
