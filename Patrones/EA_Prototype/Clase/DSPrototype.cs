﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA_Prototype.Clase
{
    public class DSPrototype : AutoPrototype
    {
        public override AutoPrototype Clonar()
        {
            return (DSPrototype)this.MemberwiseClone();
        }

        public override string VerAuto()
        {
            return $"DS {_modelo}, Color {_color}";
        }
    }
}
