﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA_Prototype.Clase
{
    public class AlfaRomeoPrototype : AutoPrototype
    {
        public override AutoPrototype Clonar()
        {
            return (AlfaRomeoPrototype)this.MemberwiseClone();
        }

        public override string VerAuto()
        {
            return $"Alfa Romeo {_modelo}, Color {_color}";
        }
    }
}
