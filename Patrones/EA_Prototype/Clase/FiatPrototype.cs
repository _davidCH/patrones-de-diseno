﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA_Prototype.Clase
{
    class FiatPrototype : AutoPrototype
    {
        public override AutoPrototype Clonar()
        {
            return (FiatPrototype)this.MemberwiseClone(); //retorna una copia superficial 
        }

        public override string VerAuto()
        {
            return $"Fiat {_modelo}, Color {_color}";
        }
    }
}
