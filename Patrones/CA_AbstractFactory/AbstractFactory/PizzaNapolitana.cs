﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_AbstractFactory.AbstractFactory
{
    public class PizzaNapolitana : Pizza
    {
        public PizzaNapolitana()
        {
            _descripcion = "Pizza Napolitana";
        }
    }
}
