﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_AbstractFactory.AbstractFactory
{
    class PizzeriaArgentina : Pizzeria
    {
        public override Pizza CrearPizza()
        {
            return new PizzaCancha();
        }
    }
}
