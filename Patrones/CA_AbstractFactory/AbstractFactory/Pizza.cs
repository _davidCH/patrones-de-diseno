﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_AbstractFactory.AbstractFactory
{
    public abstract class Pizza
    {
        protected string _descripcion;
        public object Descripcion
        {
            get
            {
                return _descripcion;
            }
        }
    }
}
