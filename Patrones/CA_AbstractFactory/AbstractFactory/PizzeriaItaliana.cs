﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_AbstractFactory.AbstractFactory
{
    public class PizzeriaItaliana : Pizzeria
    {
        public override Pizza CrearPizza()
        {
            return new PizzaNapolitana();
        }
    }
}
