﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_AbstractFactory.AbstractFactory
{
    public abstract class Pizzeria
    {
        public abstract Pizza CrearPizza();
    }
}
