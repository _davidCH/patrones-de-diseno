﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_AbstractFactory.AbstractFactory
{
    public class PizzaCancha : Pizza
    {
        public PizzaCancha()
        {
            _descripcion = "Pizza de cancha";
        }
    }
}
