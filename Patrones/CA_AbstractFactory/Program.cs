﻿using System;
using System.Data.Common;
using CA_AbstractFactory.AbstractFactory;

namespace CA_AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizzeria fabrica;
            fabrica = new PizzeriaArgentina();
            Pizza pizza = fabrica.CrearPizza();
            Console.WriteLine(pizza.Descripcion);

            fabrica = new PizzeriaItaliana();
            pizza = fabrica.CrearPizza();
            Console.WriteLine(pizza.Descripcion);

            Console.ReadKey();
        
        }
    }
}
