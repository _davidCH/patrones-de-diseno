﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FA_Adapter.Clases
{
    class MotorDiesel : Motor
    {
        public override void Acelerar()
        {
            Console.WriteLine("Acelerando motor diesel..");
        }

        public override void Arrancar()
        {
            Console.WriteLine("Arrancando motor diesel..");
        }

        public override void CargarCombustible()
        {
            Console.WriteLine("Cargando combustible motor diesel.."); ;
        }

        public override void Detener()
        {
            Console.WriteLine("Deteniendo motor diesel..");
        }
    }
}
