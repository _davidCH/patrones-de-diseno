﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FA_Adapter.Clases
{
    class MotorNaftero : Motor 
    {
        public override void Acelerar()
        {
            Console.WriteLine("Acelerando motor naftero..");
        }

        public override void Arrancar()
        {
            Console.WriteLine("Arrancando motor naftero..");
        }

        public override void CargarCombustible()
        {
            Console.WriteLine("Cargando combustible motor naftero.."); ;
        }

        public override void Detener()
        {
            Console.WriteLine("Deteniendo motor naftero..");
        }
    }
}
