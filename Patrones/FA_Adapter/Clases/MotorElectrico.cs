﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FA_Adapter.Clases
{
    class MotorElectrico
    {
        bool _conectado;
        bool _activo;
        bool _moviendo;

        public void Conectar()
        {
            if (_conectado)
                Console.WriteLine("Imposible conectar un motor electrico ya conectado");
            else
            {
                _conectado = true;
                Console.WriteLine("Motor conectado");
            }
        }
        public void Activar()
        {
            if(!_conectado)
                Console.WriteLine("Motor electrico activado");
            else
            {
                _activo = true;
                Console.WriteLine("Motor electrico activado");
            }
        }
        public void Mover()
        {
            if(_conectado && _activo)
            {
                _moviendo = true;
                Console.WriteLine("Moviendo vehiculo con motor electrico");
            }
            else
            {
                Console.WriteLine("El motor deberá estar conectado y activo");
            }            
        }
        public void Parar()
        {
            _moviendo = false;
            Console.WriteLine("Motor electrico parado");
        }
        public void Desconectar()
        {
            _conectado = false;
            Console.WriteLine("Motor electrico Desconectado");
        }
        public void Desactivar()
        {
            _activo = false;
            Console.WriteLine("Motor electrico desactivado");
        }
        public void Enchufar()
        {
            if (!_activo)
            {
                _activo = false;
                Console.WriteLine("Motor cargando las baterias..");
            }
            else
            {
                Console.WriteLine("Imposible enchufar un motor activo");
            }
        }
    }
}
