﻿using System;
using FA_Adapter.Clases;

namespace FA_Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            MotorNaftero m1 = new MotorNaftero();
            m1.Arrancar();
            m1.Acelerar();
            m1.Detener();
            m1.CargarCombustible();

            MotorDiesel m2 = new MotorDiesel();
            m2.Arrancar();
            m2.Acelerar();
            m2.Detener();
            m2.CargarCombustible();

            MotorElectricoAdapter m3 = new MotorElectricoAdapter();
            m3.Arrancar();
            m3.Acelerar();
            m3.Detener();
            m3.CargarCombustible();
        }
    }
}
